
<h2><?php echo $header; ?></h2>
<hr/>

<div style="margin-bottom: 5px;">
   <button type="button" class="btn btn-default" onclick="App.Nivel.FormNovo();" > 
      <i class="fa fa-floppy-o"></i> Adicionar 
   </button>
</div>

<div class="table-responsive">
   <table class="table table-striped">
      <thead>
         <tr>           
            <th><?php echo $this->Paginator->sort('id', 'ID') ?></th>
            <th><?php echo $this->Paginator->sort('nome') ?></th>
            <th><?php echo $this->Paginator->sort('created', 'Criado em') ?></th>
            <th>Ações</th>
         </tr>
      </thead>
      <tbody>
         <?php foreach ($nivels as $nivel): ?>
             <tr>
                <td><?= $this->Number->format($nivel->id) ?></td>
                <td><?= h($nivel->nome) ?></td>
                <td><?= h($nivel->created) ?></td>
                <td>
                   <button type="button" class="btn btn-default btn-sm" onclick="App.Nivel.FormEditar('<?php echo $this->Number->format($nivel->id); ?>');"  title="Editar"><i class="fa fa-pencil"></i></button>
                   &nbsp;
                   <button type="button" class="btn btn-default btn-sm" onclick="App.Nivel.Excluir('<?php echo $this->Number->format($nivel->id); ?>')" title="Excluir"><i class="fa fa-times"></i></button>
                </td>
             </tr>
         <?php endforeach; ?>           
      </tbody>
   </table>
</div>

<!-- Modais -->
<div id="modalNivel" tabindex="-1" role="dialog" aria-hidden="true" aria-labbeledby="MyModal1Label" class="modal fade">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header ">
            <h3 class="modal-title" id="MyModal1Label">Niveis</h3>
            <label id="tipoModal">Novo</label>
         </div>
         <div class="modal-body ">    
            <form id="form-nivel" method="post" accept-charset="utf-8" class="form-horizontal" role="form" action="#">
               <input type="hidden" name="id" id="id" value="0" class="form-control" />

               <div class="form-group text">
                  <label class="col-sm-4 col-md-2 control-label"  for="sala">Nome</label>
                  <div class="col-sm-4 col-md-6">
                     <input type="text" name="nome" id="nome" class="form-control" maxlength="100"/>
                  </div>
               </div>
            </form>
         </div>
         <div class="modal-footer ">
            <button class="btn btn-primary btn-primary" type="submit" onclick="App.Nivel.Salvar()">Salvar</button>
            <button data-dismiss="modal" class="btn btn-default" type="submit">Fechar</button>
         </div>
      </div>
   </div>
</div>

