<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Nivel $nivel
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Nivel'), ['action' => 'edit', $nivel->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Nivel'), ['action' => 'delete', $nivel->id], ['confirm' => __('Are you sure you want to delete # {0}?', $nivel->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Nivels'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Nivel'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tarefas'), ['controller' => 'Tarefas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tarefa'), ['controller' => 'Tarefas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="nivels view large-9 medium-8 columns content">
    <h3><?= h($nivel->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nome') ?></th>
            <td><?= h($nivel->nome) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($nivel->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($nivel->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($nivel->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Tarefas') ?></h4>
        <?php if (!empty($nivel->tarefas)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Nome') ?></th>
                <th scope="col"><?= __('Descricao') ?></th>
                <th scope="col"><?= __('Concluido') ?></th>
                <th scope="col"><?= __('Excluido') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($nivel->tarefas as $tarefas): ?>
            <tr>
                <td><?= h($tarefas->id) ?></td>
                <td><?= h($tarefas->user_id) ?></td>
                <td><?= h($tarefas->nome) ?></td>
                <td><?= h($tarefas->descricao) ?></td>
                <td><?= h($tarefas->concluido) ?></td>
                <td><?= h($tarefas->excluido) ?></td>
                <td><?= h($tarefas->created) ?></td>
                <td><?= h($tarefas->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Tarefas', 'action' => 'view', $tarefas->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Tarefas', 'action' => 'edit', $tarefas->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Tarefas', 'action' => 'delete', $tarefas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tarefas->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
