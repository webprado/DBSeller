<!--<h1>Login</h1>-->
<?php //= $this->Form->create() ?>
<?php //= $this->Form->control('username') ?>
<?php //= $this->Form->control('password') ?>
<?php //= $this->Form->button('Login') ?>
<?php //= $this->Form->end() ?>

<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="https://www.dbseller.com.br/wp-content/uploads/2014/05/favicon.png" type="image/png">
        <?php
        echo $this->Html->charset();
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        echo $this->Html->css('bootstrap.min.css');
        echo $this->Html->css('custom.css');
        ?>
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

        <?php
        
        echo $this->Html->script('bootstrap.min.js');
        ?>

        <link href="http://getbootstrap.com/2.3.2/assets/css/bootstrap.css" rel="stylesheet">


        <style>

            body {
                padding-top: 40px;
                padding-bottom: 40px;
                background-color: #f5f5f5;
            }

            .form-signin {
                max-width: 300px;
                padding: 19px 29px 29px;
                margin: 0 auto 20px;
                background-color: #fff;
                border: 1px solid #e5e5e5;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
            }
            .form-signin .form-signin-heading,
            .form-signin .checkbox {
                margin-bottom: 10px;
            }
            .form-signin input[type="text"],
            .form-signin input[type="password"] {
                font-size: 16px;
                height: auto;
                margin-bottom: 15px;
                padding: 7px 9px;
            }

            h2 { font-size: 20px; }
        </style>

        <link href="http://getbootstrap.com/2.3.2/assets/css/bootstrap-responsive.css" rel="stylesheet">

    </head>
    <body >

        <div class="container">
            <form action="login" class="form-signin" id="UserLoginForm" method="post" accept-charset="utf-8">                
                <div style="text-align:center;">
                   
                    <img src="https://www.dbseller.com.br/wp-content/uploads/2014/05/LogoDBseller1.png" id="logoTopo" alt="">
                   
                    <h2 class="form-signin-heading">Gerenciador de Tarefas<br>AppTasks</h2>
                </div>

                <input type="text" class="input-block-level" placeholder="Login-Users" name="username" id="username">
                <input type="password" class="input-block-level" placeholder="Senha" name="password" id="password">

                <div style="text-align:center;">
                    <button class="btn btn-default btn-primary" type="submit"><i class="fa fa-check-circle-o"></i>&nbsp;Entrar</button>
                </div>
            </form>
        </div>

    </body>
</html>