<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Tarefa $tarefa
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Tarefa'), ['action' => 'edit', $tarefa->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Tarefa'), ['action' => 'delete', $tarefa->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tarefa->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Tarefas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tarefa'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Nivels'), ['controller' => 'Nivels', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Nivel'), ['controller' => 'Nivels', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tarefas view large-9 medium-8 columns content">
    <h3><?= h($tarefa->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $tarefa->has('user') ? $this->Html->link($tarefa->user->id, ['controller' => 'Users', 'action' => 'view', $tarefa->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nome') ?></th>
            <td><?= h($tarefa->nome) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($tarefa->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Concluido') ?></th>
            <td><?= $this->Number->format($tarefa->concluido) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Excluido') ?></th>
            <td><?= $this->Number->format($tarefa->excluido) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($tarefa->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($tarefa->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($tarefa->descricao)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Nivels') ?></h4>
        <?php if (!empty($tarefa->nivels)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Nome') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($tarefa->nivels as $nivels): ?>
            <tr>
                <td><?= h($nivels->id) ?></td>
                <td><?= h($nivels->nome) ?></td>
                <td><?= h($nivels->created) ?></td>
                <td><?= h($nivels->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Nivels', 'action' => 'view', $nivels->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Nivels', 'action' => 'edit', $nivels->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Nivels', 'action' => 'delete', $nivels->id], ['confirm' => __('Are you sure you want to delete # {0}?', $nivels->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
