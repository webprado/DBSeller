
<h2><?php echo $header; ?></h2>
<hr/>

<div style="margin-bottom: 5px;">
   <button type="button" class="btn btn-default" onclick="App.Tarefa.FormNovo()" > 
      <i class="fa fa-floppy-o"></i> Adicionar 
   </button>
</div>

<div class="table-responsive">
   <table class="table table-striped">
      <thead>
         <tr>           
            <th><?php echo $this->Paginator->sort('id', 'ID') ?></th>                
            <th><?php echo $this->Paginator->sort('nome') ?></th>
            <th><?php echo $this->Paginator->sort('descricao', 'Descrição') ?></th>
            <th><?php echo $this->Paginator->sort('user_nome', 'Criado por') ?></th>
            <th><?php echo $this->Paginator->sort('created', 'Criado em') ?></th>
            <th>Nível</th>
            <th>Ações</th>
         </tr>
      </thead>
      <tbody>
         <?php
         foreach ($tarefas as $tarefa):
              if($tarefa->excluido == 1) continue;
             ?>
             <tr>
                <td><?= $this->Number->format($tarefa->id) ?></td>                
                <td><?= h($tarefa->nome) ?></td>
                <td><?= h($tarefa->descricao) ?></td>
                <td><?= h($tarefa->user->nome) ?></td>
                <td><?= h($tarefa->created) ?></td>
                <td>
                   <select onchange="App.Tarefa.AssociarNivel(this,'<?php echo $this->Number->format($tarefa->id)?>')">
                      <?php
                      foreach ($listaNiveis as $nivel) {
                          $selected = $nivel->id == $tarefa->nivel_id ? 'selected' : "";
                          echo "<option  {$selected}  value=\"{$nivel->id}\">{$nivel->nome}</option>";
                      }
                      ?>
                   </select>
                </td>
                <td>
                   <button type="button" class="btn btn-default btn-sm" onclick="App.Tarefa.FormEditar('<?php echo $this->Number->format($tarefa->id); ?>');"  title="Editar"><i class="fa fa-pencil"></i></button>
                   &nbsp;
                   <button type="button" class="btn btn-default btn-sm" onclick="App.Tarefa.Excluir('<?php echo $this->Number->format($tarefa->id); ?>')" title="Excluir"><i class="fa fa-times"></i></button>
                </td>
             </tr>
         <?php endforeach; ?>           
      </tbody>
   </table>   
</div>

<!-- Modais -->
<div id="modalTarefa" tabindex="-1" role="dialog" aria-hidden="true" aria-labbeledby="MyModal1Label" class="modal fade">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header ">
            <h3 class="modal-title" id="MyModal1Label">Tarefas</h3>
            <label id="tipoModal">Novo</label>
         </div>
         <div class="modal-body ">    
            <form id="form-tarefa" method="post" accept-charset="utf-8" class="form-horizontal" role="form" action="#">
               <input type="hidden" name="id" id="id" value="0" class="form-control" />               
               <input type="hidden" name="concluido" id="concluido" value="0" class="form-control" />
               <input type="hidden" name="excluido" id="excluido" value="0" class="form-control" />
               <input type="hidden" name="user_id" id="user_id" value="<?php echo $UsuarioID;?>" class="form-control" />

               <div class="form-group text">
                  <label class="col-sm-4 col-md-2 control-label"  for="sala">Nível</label>
                  <div class="col-sm-4 col-md-6">
                     <div id="comboNiveis"></div>
                  </div>
               </div>
               
               <div class="form-group text">
                  <label class="col-sm-4 col-md-2 control-label"  for="sala">Nome</label>
                  <div class="col-sm-4 col-md-6">
                     <input type="text" name="nome" id="nome" class="form-control" maxlength="100"/>
                  </div>
               </div>

               <div class="form-group text">
                  <label class="col-sm-4 col-md-2 control-label"  for="sala">Nome</label>
                  <div class="col-sm-4 col-md-6">
                     <textarea name="descricao" id="descricao" class="form-control" rows="6"></textarea>
                  </div>
               </div>
            </form>
         </div>
         <div class="modal-footer ">
            <button class="btn btn-primary btn-primary" type="submit" onclick="App.Tarefa.Salvar()">Salvar</button>
            <button data-dismiss="modal" class="btn btn-default" type="submit">Fechar</button>
         </div>
      </div>
   </div>
</div>
