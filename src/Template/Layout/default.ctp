<?php
$cakeDescription = 'DBSeller | Gestão de Tarefas';
?>
<!DOCTYPE html>
<html lang="pt-BR">
   <head>
      <?= $this->Html->charset() ?>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="shortcut icon" href="https://www.dbseller.com.br/wp-content/uploads/2014/05/favicon.png" type="image/png">
      <title>
         <?= $cakeDescription ?>:
         <?= $this->fetch('title') ?>
      </title>
      <?= $this->Html->meta('icon') ?>

      <?php //= $this->Html->css('base.css')  ?>
      <?php //= $this->Html->css('cake.css') ?>

      <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

      <?= $this->fetch('meta') ?>
      <?= $this->fetch('css') ?>
      <?= $this->fetch('script') ?>
      <?= $this->Html->css('bootstrap.min.css') ?>
      <?= $this->Html->css('custom.css') ?>
   </head>
   <body>

      <nav class="navbar navbar-default navbar-fixed-top">
         <div class="container">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand" href="#">AppTasks</a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
               <ul class="nav navbar-nav">
                  <?php if (!empty($classMenuAtivoTarefa) || !empty($classMenuAtivoNivel)) { ?>
                      <li class="<?php echo $classMenuAtivoTarefa; ?>"><a href="Tarefas">Tarefas</a></li>
                      <li class="<?php echo $classMenuAtivoNivel; ?>"><a href="Nivels">Niveis</a></li>
                  <?php } ?>
               </ul>
               <?php if (!empty($classMenuAtivoTarefa) || !empty($classMenuAtivoNivel)) { ?>
                   <ul class="nav navbar-nav navbar-right">
                      <li class="active">
                         <a href="users/logout">SAIR </a></li>
                   </ul>
               <?php } ?>
            </div><!--/.nav-collapse -->
         </div>
      </nav>

      <div class="container">
         <?= $this->Flash->render() ?>
         <?= $this->fetch('content') ?>         
      </div>

      <footer class="footer">
         <div class="container">
            <p class="text-muted">Rodrigo Prado &copy</p>
         </div>
      </footer>

      <?php
      echo $this->Html->script('jquery.min.js');
      echo $this->Html->script('bootstrap.min.js');
      echo $this->Html->script('app.js');
      ?>

   </body>
</html>
