<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class TarefasController extends AppController {

    public $arrNiveis;

    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
        $this->set('header', 'Tarefas');
        $this->arrNiveis = [];
    }

    public function isAuthorized($user) {
        $action = $this->request->getParam('action');

        $this->set('UsuarioID', $this->Auth->user('id'));
       
        // The add and index actions are always allowed.
        if (in_array($action, ['index', 'add', 'edit', 'delete', 'carregarDados', 'carregarNiveis', 'associarNivel'])) {
            return true;
        }
        // All other actions require an id.
        if (!$this->request->getParam('pass.0')) {
            return false;
        }
        
        return parent::isAuthorized($user);
    }

    public function index() {

        $this->set('classMenuAtivoTarefa', 'active');
        $this->set('classMenuAtivoNivel', '');

        $this->loadModel('Nivels');
        $listaNiveis = $this->Nivels->find('all', [
            'conditions' => ['Nivels.excluido' => 0],
            'order' => 'Nivels.nome ASC'
        ]);

        $this->paginate = [
            'contain' => ['Users']
        ];
        $tarefas = $this->paginate($this->Tarefas);

        $this->set(compact('listaNiveis'));
        $this->set(compact('tarefas'));
        $this->set('_serialize', ['tarefas2']);
    }

    public function view($id = null) {
        $tarefa = $this->Tarefas->get($id, [
            'contain' => ['Users', 'Nivels']
        ]);

        $this->set('tarefa', $tarefa);
        $this->set('_serialize', ['tarefa']);
    }

    public function add() {
        $tarefa = $this->Tarefas->newEntity();

        if ($this->request->is('post')) {

            $tarefa = $this->Tarefas->patchEntity($tarefa, $this->request->getData());

            //debug($tarefa);

            if ($this->Tarefas->save($tarefa)) {

                $this->Flash->success(__('A tarefa foi salva.'));
                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('A tarefa não pôde ser salva. Por favor, tente novamente.'));
        }
        $users = $this->Tarefas->Users->find('list', ['limit' => 200]);
        $this->set(compact('tarefa', 'users'));
        $this->set('_serialize', ['tarefa']);
    }

    public function edit($id = null) {
        $tarefa = $this->Tarefas->get($id, [
            'contain' => ['Nivels']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tarefa = $this->Tarefas->patchEntity($tarefa, $this->request->getData());
            if ($this->Tarefas->save($tarefa)) {
                $this->Flash->success(__('A tarefa foi salva.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('A tarefa não pôde ser salva. Por favor, tente novamente.'));
        }
        $users = $this->Tarefas->Users->find('list', ['limit' => 200]);
        $nivels = $this->Tarefas->Nivels->find('list', ['limit' => 200]);
        $this->set(compact('tarefa', 'users', 'nivels'));
        $this->set('_serialize', ['tarefa']);
    }

    public function delete($id = null) {

        $tarefa = $this->Tarefas->get($id);

        if ($this->request->is('get')) {

            $tarefa = $this->Tarefas->patchEntity($tarefa, ['excluido' => 1]);

            if ($this->Tarefas->save($tarefa)) {
                $this->Flash->success(__('A tarefa foi excluida.'));
            } else {
                $this->Flash->error(__('A tarefa não pôde ser salva. Por favor, tente novamente.'));
            }

            return $this->redirect(['action' => 'index']);
        }
    }

    public function carregarDados($id = null) {

        $tarefa = $this->Tarefas->get($id);

        $this->carregarNiveis(true, $tarefa->nivel_id);

        if ($tarefa) {
            $arrDados = array();
            $arrDados['id'] = $tarefa->id;
            $arrDados['nome'] = $tarefa->nome;
            $arrDados['descricao'] = $tarefa->descricao;
            $arrDados['niveis'] = $this->arrNiveis;
            $jsonDados = json_encode($arrDados);

            echo $jsonDados;
            die;
        }
    }

    public function carregarNiveis($interna = false, $idNivelTarefa = null) {
        $arrNiveis = [];
        $this->loadModel('Nivels');
        $listaNiveis = $this->Nivels->find('all', [
            'conditions' => ['Nivels.excluido' => 0],
            'order' => 'Nivels.nome ASC'
                ]
        );
        foreach ($listaNiveis as $nivel) {
            $arrTmp = [
                'id' => $nivel->id,
                'nome' => $nivel->nome,
                'selected' => $nivel->id == $idNivelTarefa ? "selected" : ""
            ];

            array_push($arrNiveis, $arrTmp);
        }

        if ($interna) {
            $this->arrNiveis = $arrNiveis;
        } else {
            echo json_encode($arrNiveis);
            die;
        }
    }

    public function associarNivel() {

        $tarefa = $this->Tarefas->get($this->request->getData()['id'], [
            'contain' => ['Nivels']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tarefa = $this->Tarefas->patchEntity($tarefa, $this->request->getData());
            if ($this->Tarefas->save($tarefa)) {
                $this->Flash->success(__('A tarefa foi associada ao nível.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('A tarefa não pôde ser associada ao nível. Por favor, tente novamente.'));
        }
    }

}
