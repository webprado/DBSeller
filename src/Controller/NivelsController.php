<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Controller\Component\FlashComponent;

class NivelsController extends AppController {

    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
        $this->set('header', 'Níveis');
    }

    public function isAuthorized($user) {

        $action = $this->request->getParam('action');

        // The add and index actions are always allowed.
        if (in_array($action, ['index', 'add', 'edit', 'delete', 'carregarDados'])) {
            return true;
        }
        // All other actions require an id.
        if (!$this->request->getParam('pass.0')) {
            return false;
        }

        return parent::isAuthorized($user);
    }

    public function index() {

        $this->set('classMenuAtivoTarefa', '');
        $this->set('classMenuAtivoNivel', 'active');

        $this->paginate = [
            'conditions' => [
                'Nivels.excluido' => 0,
            ]
        ];

        $nivels = $this->paginate($this->Nivels);

        $this->set(compact('nivels'));
        $this->set('_serialize', ['nivels']);
    }

    public function add() {
        $nivel = $this->Nivels->newEntity();

        if ($this->request->is('post')) {
            $nivel = $this->Nivels->patchEntity($nivel, $this->request->getData());
            if ($this->Nivels->save($nivel)) {
                $this->Flash->success(__('O nível foi salvo.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('O nivel não pôde ser salvo. Por favor, tente novamente.'));
        }
        $tarefas = $this->Nivels->Tarefas->find('list', ['limit' => 200]);
        $this->set(compact('nivel', 'tarefas'));
        $this->set('_serialize', ['nivel']);
    }

    public function edit($id = null) {
        $nivel = $this->Nivels->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $nivel = $this->Nivels->patchEntity($nivel, $this->request->getData());
            if ($this->Nivels->save($nivel)) {
                $this->Flash->success(__('O nível foi salvo.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('O nivel não pôde ser salvo. Por favor, tente novamente.'));
        }
        $tarefas = $this->Nivels->Tarefas->find('list', ['limit' => 200]);
        $this->set(compact('nivel', 'tarefas'));
        $this->set('_serialize', ['nivel']);
    }

    public function delete($id = null) {
        $nivel = $this->Nivels->get($id);

        if ($this->request->is('get')) {

            $nivel = $this->Nivels->patchEntity($nivel, ['excluido' => 1]);

            if ($this->Nivels->save($nivel)) {
                $this->Flash->success(__('O nível foi excluído.'));
            } else {
                $this->Flash->error(__('O nivel não pôde ser excluído. Por favor, tente novamente.'));
            }

            return $this->redirect(['action' => 'index']);
        }
    }

    public function carregarDados($id = null) {

        $nivel = $this->Nivels->get($id);

        if ($nivel) {
            $arrDados = array();
            $arrDados['id'] = $nivel->id;
            $arrDados['nome'] = $nivel->nome;
            $jsonDados = json_encode($arrDados);

            echo $jsonDados;
            die;
        }
    }

}
