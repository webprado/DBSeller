<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Nivels Model
 *
 * @property \App\Model\Table\TarefasTable|\Cake\ORM\Association\BelongsToMany $Tarefas
 *
 * @method \App\Model\Entity\Nivel get($primaryKey, $options = [])
 * @method \App\Model\Entity\Nivel newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Nivel[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Nivel|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Nivel patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Nivel[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Nivel findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class NivelsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('nivels');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Tarefas', [
            'foreignKey' => 'nivel_id',
            'targetForeignKey' => 'tarefa_id',
            'joinTable' => 'nivels_tarefas'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            //->scalar('nome')
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');
        
        return $validator;
    }
}
