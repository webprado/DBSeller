
var getUrl = window.location;
var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

var comboNiveis;

var App = {
   Nivel: {
      FormNovo: function () {
         $('#id, #nome').val('');
         $('#id').val('0');
         $('#tipoModal').html('Novo');
         $('#modalNivel').modal('show');
      },
      FormEditar: function (id) {
         $('#tipoModal').html('Edição');

         $.ajax({
            type: 'GET',
            url: baseUrl + '/nivels/carregarDados/' + id,
            data: '',
            dataType: 'json'
         }).done(function (dados) {

            console.log(dados);

            $.each(dados, function (campo, valor) {
               $('#' + campo).val(valor);
            });

            $('#modalNivel').modal('show');

         }).fail(function (jqXHR, textStatus) {
            alert('Erro: ' + textStatus + '\n' + jqXHR);
            console.log(textStatus);
            console.log(jqXHR.responseText);

         });
      },
      Salvar: function () {

         var URL = '';

         if ($('#id').val() != 0 && $('#id').val() != '' && $('#id').val() != undefined)
            URL = baseUrl + '/nivels/edit/' + $('#id').val();
         else
            URL = baseUrl + '/nivels/add/';

         var dados = $('#form-nivel').serialize();

         $.ajax({
            type: 'POST',
            url: URL,
            data: dados,
            dataType: 'text'

         }).done(function (data) {
            
            location.reload(true);

         }).fail(function (jqXHR, textStatus) {
            alert('Erro: ' + textStatus + '\n' + jqXHR);
            console.log(textStatus);
            console.log(jqXHR.responseText);

         });
      },
      Excluir: function (id) {

         if (confirm('Deseja excluir O Nível?\nPode haver tarefas associdas, desta forma estas não ficaram visíveis')) {

            $.ajax({
               type: 'GET',
               url: baseUrl + '/nivels/delete/' + id,
               data: '',
               dataType: 'text'
            }).done(function (dados) {
               location.reload(true);

            }).fail(function (jqXHR, textStatus) {
               alert('Erro: ' + textStatus + '\n' + jqXHR);
               console.log(textStatus);
               console.log(jqXHR.responseText);

            });
         }
      }
   },
   Tarefa: {
      CarregaComboNiveis: function () {

         $.ajax({
            type: 'GET',
            url: baseUrl + '/tarefas/carregarNiveis/',
            data: '',
            dataType: 'json'
         }).done(function (dados) {

            if (dados != null) {

               var htmlCombo = '';
               if (dados.length > 0) {
                  htmlCombo += '<select id="nivel_id" name="nivel_id" class="form-control">';
                  $.each(dados, function (index, obj) {
                     htmlCombo += '<option value="' + obj.id + '" ' + obj.selected + ' > ' + obj.nome + ' </option>';
                  })
                  htmlCombo += '</select>';

                  $('#comboNiveis').html(htmlCombo);
               }
            }

         }).fail(function (jqXHR, textStatus) {
            alert('Erro: ' + textStatus + '\n' + jqXHR);
            console.log(textStatus);
            console.log(jqXHR.responseText);

         });

      },
      FormNovo: function () {
         //$('#id, #nome').val('');
         $('#id').val('0');         
         $('#tipoModal').html('Novo');

         this.CarregaComboNiveis();

         $('#modalTarefa').modal('show');
      },
      FormEditar: function (id) {
         $('#tipoModal').html('Edição');

         $.ajax({
            type: 'GET',
            url: baseUrl + '/tarefas/carregarDados/' + id,
            data: '',
            dataType: 'json'
         }).done(function (dados) {

            $.each(dados, function (campo, valor) {
               $('#' + campo).val(valor);
            });

            var htmlCombo = '';
            if (dados.niveis.length > 0) {
               htmlCombo += '<select id="nivel_id" name="nivel_id" class="form-control">';
               $.each(dados.niveis, function (index, obj) {
                  htmlCombo += '<option value="' + obj.id + '" ' + obj.selected + ' > ' + obj.nome + ' </option>';
               })
               htmlCombo += '</select>';

               $('#comboNiveis').html(htmlCombo);
            }

            $('#modalTarefa').modal('show');

         }).fail(function (jqXHR, textStatus) {
            alert('Erro: ' + textStatus + '\n' + jqXHR);
            console.log(textStatus);
            console.log(jqXHR.responseText);

         });
      },
      Salvar: function () {

         var URL = '';

         if ($('#id').val() != 0 && $('#id').val() != '' && $('#id').val() != undefined)
            URL = baseUrl + '/tarefas/edit/' + $('#id').val();
         else
            URL = baseUrl + '/tarefas/add/';

         var dados = $('#form-tarefa').serialize();
         
         console.log(dados +'==='+ URL);

         $.ajax({
            type: 'POST',
            url: URL,
            data: dados,
            dataType: 'text'

         }).done(function (data) {

            //console.log(data);
            location.reload(true);

         }).fail(function (jqXHR, textStatus) {
            alert('Erro: ' + textStatus + '\n' + jqXHR);
            console.log(textStatus);
            console.log(jqXHR.responseText);

         });
      },
      AssociarNivel: function (obj, tarefaId) {

         var dados = {
            id: tarefaId,
            nivel_id: $(obj).val()
         }

         $.ajax({
            type: 'POST',
            url: baseUrl + '/tarefas/associarNivel/',
            data: dados,
            dataType: 'text'

         }).done(function (data) {

            location.reload(true);

         }).fail(function (jqXHR, textStatus) {
            alert('Erro: ' + textStatus + '\n' + jqXHR);
            console.log(textStatus);
            console.log(jqXHR.responseText);

         });
      },
      Excluir: function (id) {

         if (confirm('Deseja excluir a tarefa?')) {

            $.ajax({
               type: 'GET',
               url: baseUrl + '/tarefas/delete/' + id,
               data: '',
               dataType: 'text'
            }).done(function (dados) {
               location.reload(true);

            }).fail(function (jqXHR, textStatus) {
               alert('Erro: ' + textStatus + '\n' + jqXHR);
               console.log(textStatus);
               console.log(jqXHR.responseText);

            });
         }
      }
   }
}
