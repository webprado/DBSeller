-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.24-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table appteste.nivels
CREATE TABLE IF NOT EXISTS `nivels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT '',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `excluido` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- Dumping data for table appteste.nivels: ~2 rows (approximately)
/*!40000 ALTER TABLE `nivels` DISABLE KEYS */;
INSERT INTO `nivels` (`id`, `nome`, `created`, `modified`, `excluido`) VALUES
	(19, 'Teste Nível', '2017-08-29 23:43:47', '2017-08-29 23:43:47', 0),
	(20, 'Outro Nível', '2017-08-29 23:48:54', '2017-08-29 23:48:54', 0);
/*!40000 ALTER TABLE `nivels` ENABLE KEYS */;


-- Dumping structure for table appteste.tarefas
CREATE TABLE IF NOT EXISTS `tarefas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `nivel_id` int(11) DEFAULT NULL,
  `nome` varchar(100) NOT NULL,
  `descricao` text NOT NULL,
  `concluido` int(11) NOT NULL DEFAULT '0',
  `excluido` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK1_user_id` (`user_id`),
  KEY `FK2_nivel_id` (`nivel_id`),
  CONSTRAINT `FK1_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FK2_nivel_id` FOREIGN KEY (`nivel_id`) REFERENCES `nivels` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table appteste.tarefas: ~0 rows (approximately)
/*!40000 ALTER TABLE `tarefas` DISABLE KEYS */;
INSERT INTO `tarefas` (`id`, `user_id`, `nivel_id`, `nome`, `descricao`, `concluido`, `excluido`, `created`, `modified`) VALUES
	(4, 1, 20, 'Tarefa 11', ',jcvjcvjncv', 0, 0, '2017-08-30 00:42:30', '2017-08-30 00:42:30'),
	(5, 1, 20, 'Outra tarefa', 'sfsdfjbdssdj dsinsdsdnsdilsd ', 0, 0, '2017-08-30 00:43:57', '2017-08-30 00:44:43'),
	(6, 1, 20, 'Outra 22', 'sasd', 0, 1, '2017-08-30 00:44:29', '2017-08-30 00:44:56');
/*!40000 ALTER TABLE `tarefas` ENABLE KEYS */;


-- Dumping structure for table appteste.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table appteste.users: ~1 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `nome`, `username`, `password`, `created`, `modified`) VALUES
	(1, 'Rodrigo', 'prado', '$2y$10$Ofd7JMDavkHztD6fZNakaOiZguMoV7TPAWzG11ss6SSMS/zQS3KBG', '2017-08-29 23:09:20', '2017-08-29 23:09:20'),
	(2, 'Andrea', 'andrea', '$2y$10$TEMSfWL59lTZtMXU0LJU4.mG7JN67Bc5ohqqu4q7PiZUVd/27nISq', '2017-08-30 00:57:41', '2017-08-30 00:57:41');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
